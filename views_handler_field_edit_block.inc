<?php

/**
 * @file
 * Definition of views_handler_field_edit_block.
 */

/**
 * Field handler to present a link to block edit.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_edit_block extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['module'] = 'module';
    $this->additional_fields['delta'] = 'delta';
  }
  function query() {
    //$this->ensure_my_table();
    $this->add_additional_fields();
  }    
  function option_definition() {
    $options = parent::option_definition();
    $options['text'] = array('default' => '', 'translatable' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );
    parent::options_form($form, $form_state);

    // The path is set by render_link function so don't allow to set it.
    $form['alter']['path'] = array('#access' => FALSE);
    $form['alter']['external'] = array('#access' => FALSE);
  }

  function render($values) {
    
    if ($values) {
      return $this->render_link($values);
    }
  }




  /**
   * Renders the link.
   */
  function render_link($values) {
    // Ensure user has access to edit this node.
    /*
    if (!user_access('administer blocks')) {
      return;
    }*/
    $this->options['alter']['make_link'] = TRUE;
    $this->options['alter']['path'] = "admin/structure/block/manage/$values->module/$values->delta/configure";
    $this->options['alter']['query'] = drupal_get_destination();

    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');
    return $text;
  }
}
