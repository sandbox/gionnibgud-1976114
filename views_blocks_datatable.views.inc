<?php

/**
   * Implements hook_views_data().
   */
  function views_blocks_datatable_views_data() {
    // setup basic data table
    $data = array(
      'block' => array(
        'table' => array(
          'group' => t('Blocks'),
        'base' => array(
          'field' => 'bid',
          'title' => t('Block'),
          'help' => t("Drupal blocks from core Block module."),
          'weight' => -10,
          ), 
        ),
      ),
    );
    // setup fields
    $data['block']['bid'] = array(
        'title'     => t('Bid'),
        'help'      => t('The Block ID of the block.'),
        'field'    => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE,
          ),
        'argument' => array(
          'handler' => 'views_handler_argument_numeric',
          'name field' => 'title', // the field to display in the summary.
          'numeric' => TRUE,
          ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
          ),
        'sort' => array(
          'handler' => 'views_handler_sort',
          ),
    );
    $data['block']['title'] = array(
        'title'     => t('Title'),
        'help'      => t('The Title of the block if set.'),
        'field'    => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE,
          ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
          ),
        'filter' => array(
          'handler' => 'views_handler_filter_string',
          ),
        'sort' => array(
          'handler' => 'views_handler_sort',
          ),
    );
    $data['block']['region'] = array(
        'title'     => t('Region'),
        'help'      => t('The Region the block is placed in.'),
        'field'    => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE,
          ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
          ),
        'filter' => array(
          'handler' => 'views_handler_filter_string',
          ),
        'sort' => array(
          'handler' => 'views_handler_sort',
          ),
    );
    // Theme
    $data['block']['theme'] = array(
      'title' => t('Theme'),
      'help' => t('The theme where the block appears.'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_block_theme',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    );
    $data['block']['status'] = array(
        'title'     => t('Status'),
        'help'      => t('Whether the block is active or not.'),
        'field'    => array(
            'handler' => 'views_handler_field_boolean',
            'click sortable' => TRUE,
          ),
        'argument' => array(
          'handler' => 'views_handler_argument',
          ),
        'filter' => array(
          'handler' => 'views_handler_filter_boolean',
          ),
        'sort' => array(
          'handler' => 'views_handler_sort',
          ),
    );
    $data['block']['module'] = array(
        'title'     => t('Module'),
        'help'      => t('Module the block is based on.'),
        'field'    => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE,
          ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
          ),
        'filter' => array(
          'handler' => 'views_handler_filter_string',
          ),
        'sort' => array(
          'handler' => 'views_handler_sort',
          ),
    );
    $data['block']['delta'] = array(
        'title'     => t('Delta'),
        'help'      => t('The block delta.'),
        'field'    => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE,
          ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
          ),
        'filter' => array(
          'handler' => 'views_handler_filter_string',
          ),
        'sort' => array(
          'handler' => 'views_handler_sort',
          ),
    );
    /* edit block link */
    $data['block']['edit_block'] = array(
      'title' => t('Edit block'),
      'help' => t('Provide a simple link to edit the block.'),
      'field' => array(
        'handler' => 'views_handler_field_edit_block',
      ),
    );       
    return $data;
  }
/**
 * Implementation of hook_views_query_substitutions().
 */
function views_blocks_datatable_views_query_substitutions() {
  global $theme_key;
  $default_theme = variable_get('theme_default', $theme_key);
  return array(
    '***CURRENT_THEME***' => $theme_key,
    '***DEFAULT_THEME***' => $default_theme,
  );
}  
/**
 * Implementation of hook_views_handlers().
 */
function views_blocks_datatable_views_handlers() {
  return array(
    'handlers' => array(
      'views_handler_field_edit_block' => array(
        'parent' => 'views_handler_field',
      ),
    'views_handler_filter_block_theme' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
    ),
  );
}